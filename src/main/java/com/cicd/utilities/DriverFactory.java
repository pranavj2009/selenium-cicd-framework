package com.cicd.utilities;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverFactory {
	private static final Map<BrowserType, Supplier<WebDriver>> driverMap = new HashMap<BrowserType, Supplier<WebDriver>>();

	private static final Supplier<WebDriver> chromeDriverSupplier = () -> {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/executables/chromedriver.exe");
		System.out.println("Creating chrome driver");
		return new ChromeDriver();
	};

	private static final Supplier<WebDriver> firefoxDriverSupplier = () -> {
		System.setProperty("webdriver.gecko.driver", "src/test/resources/executables/geckodriver.exe");
		System.out.println("Creating Firefox driver");
		return new FirefoxDriver();
	};

	static {
		driverMap.put(BrowserType.CHROME, chromeDriverSupplier);
		driverMap.put(BrowserType.FIREFOX, firefoxDriverSupplier);
	}

	public static WebDriver getDriver(BrowserType browserType) {
		return driverMap.get(browserType).get();
	}

}
