package com.cicd.rough;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import com.cicd.utilities.BrowserType;
import com.cicd.utilities.DriverFactory;

public class Main {

	public static void main(String[] args) {
		WebDriver driver = DriverFactory.getDriver(BrowserType.CHROME);
		driver.get("https://google.co.in");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.close();
		driver.quit();

	}

}
